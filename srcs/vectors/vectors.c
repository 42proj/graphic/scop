/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectors.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/20 10:49:50 by svilau            #+#    #+#             */
/*   Updated: 2018/11/08 10:12:28 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <vectors.h>

t_vec3d	new_vector(double x, double y, double z)
{
	return ((t_vec3d){x, y, z});
}

t_vec3d	vector_add(t_vec3d vect1, t_vec3d vect2)
{
	return ((t_vec3d){vect1.x + vect2.x, vect1.y + vect2.y, vect1.z + vect2.z});
}

t_vec3d	vector_substract(t_vec3d vect1, t_vec3d vect2)
{
	return ((t_vec3d){vect1.x - vect2.x, vect1.y - vect2.y, vect1.z - vect2.z});
}

t_vec3d	vector_scalar(t_vec3d vect1, double scalar)
{
	return ((t_vec3d){vect1.x * scalar, vect1.y * scalar, vect1.z * scalar});
}

t_vec3d	vector_divide(t_vec3d vect1, double scalar)
{
	return ((t_vec3d){vect1.x / scalar, vect1.y / scalar, vect1.z / scalar});
}
