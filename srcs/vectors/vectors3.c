/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectors3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 09:59:41 by svilau            #+#    #+#             */
/*   Updated: 2018/11/08 10:01:10 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <vectors.h>
#include <math.h>

t_vec3d	vector_copy(t_vec3d cpy)
{
	return ((t_vec3d){cpy.x, cpy.y, cpy.z});
}

t_vec3d	vector_normalize(t_vec3d vect1)
{
	double length;

	length = vector_length(vect1);
	return ((t_vec3d){vect1.x / length, vect1.y / length, vect1.z / length});
}
