/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/20 10:49:50 by svilau            #+#    #+#             */
/*   Updated: 2018/10/25 14:16:52 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <vectors.h>

void	rotate(t_vec3d *point, double degrees, char axis)
{
	t_vec3d tmp;

	tmp.x = point->x;
	tmp.y = point->y;
	tmp.z = point->z;
	degrees = (-1) * deg_to_radians(degrees);
	if (axis == 'z')
	{
		point->x = tmp.x * cos(degrees) - tmp.y * sin(degrees);
		point->y = tmp.x * sin(degrees) + tmp.y * cos(degrees);
	}
	else if (axis == 'x')
	{
		point->y = tmp.y * cos(degrees) - tmp.z * sin(degrees);
		point->z = tmp.y * sin(degrees) + tmp.z * cos(degrees);
	}
	else if (axis == 'y')
	{
		point->z = tmp.z * cos(degrees) - tmp.x * sin(degrees);
		point->x = tmp.z * sin(degrees) + tmp.x * cos(degrees);
	}
}
