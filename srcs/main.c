/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 09:37:02 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 13:05:28 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define STB_IMAGE_IMPLEMENTATION
#include <matrix.h>
#include <input.h>
#include <stdlib.h>
#include <matrix_dispatcher.h>
#include <setup_functions.h>
#include <parser.h>
#include <errors.h>
#include <object_manipulation.h>
#include <tga_loader.h>
#include <recurring_events.h>

/*
** Initialize the library
** Create a windowed mode window and its OpenGL context
** Make the window's context current
*/

/*
** void			update_matrix(GLfloat *modelviewmatrix, float angle)
** {
** 	modelviewmatrix[0] = cos(deg_to_radians(angle));
** 	modelviewmatrix[2] = sin(deg_to_radians(angle));
** 	modelviewmatrix[8] = -1 * sin(deg_to_radians(angle));
** 	modelviewmatrix[10] = cos(deg_to_radians(angle));
** }
*/

/*
** void			print_matrix(float *matrix, int m_width, int m_height)
** {
** 	int i;
**
** 	i = 0;
** 	while (i < (m_height * m_width))
** 	{
** 		printf("%f ", matrix[i]);
** 		if ((i + 1) % m_width == 0)
** 			printf("\n");
** 		i++;
** 	}
** }
*/

unsigned int	load_texture(void)
{
	unsigned int	texture;
	t_tga_image		loaded_image;

	loaded_image = load_tga("data/textures/cat.tga");
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	if (loaded_image.image_data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, loaded_image.header->width,
			loaded_image.header->height, 0, GL_BGR,
			GL_UNSIGNED_BYTE, loaded_image.image_data);
		glGenerateMipmap(GL_TEXTURE_2D);
		free(loaded_image.image_data);
	}
	else
		error_handler("Could not open texture file");
	return (texture);
}

void			allocate_memory(t_master *master)
{
	if (!(master->scene = malloc(sizeof(t_scene))))
		error_handler("Scene struct malloc");
	if (!(master->scene->camera = malloc(sizeof(t_camera))))
		error_handler("Camera struct malloc");
	if (!(master->scene->objects = malloc(sizeof(t_object))))
		error_handler("Objects struct malloc");
}

void			render_loop(t_master *master, t_shader *shaders, t_mesh *mesh1)
{
	while (!glfwWindowShouldClose(master->window.id))
	{
		shader_bind(*shaders);
		handle_input(&master->window, master->scene);
		handle_recurring_events(&master->window, master->scene, shaders);
		send_translation_matrix(shaders, master->scene->translation_matrix);
		send_rotation_matrix(shaders, master->scene->rotation_matrix);
		send_scaling_matrix(shaders, master->scene->scaling_matrix);
		send_projection_matrix(shaders, master->scene->projection_matrix);
		send_view_matrix(shaders, master->scene->view_matrix);
		clear_window(0.86, 0.86, 0.86, 1);
		draw(mesh1);
		glfwSwapBuffers(master->window.id);
		glfwPollEvents();
	}
}

int				main(int ac, char **argv)
{
	t_master		master;
	t_shader		shaders;
	t_mesh			mesh1;
	unsigned int	texture;

	allocate_memory(&master);
	data_setup(&master);
	if (init_window(&(master.window)) < 0)
		return (-1);
	if (ac == 2)
	{
		load_object(&master.scene->objects[0], argv[1]);
		center_object(master.scene, &master.scene->objects[0]);
		texture = load_texture();
		load_shaders(&shaders, "./srcs/shaders/vertex.vtx",
								"./srcs/shaders/fragment.frg");
		mesh(&mesh1, master.scene->objects[0]);
	}
	else
		error_handler("Not enough arguments\nUsage : ./scope [map_name]");
	render_loop(&master, &shaders, &mesh1);
	glfwTerminate();
	return (0);
}
