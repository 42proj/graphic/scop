/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup_dispatcher.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 12:34:23 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 10:25:30 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <setup_functions.h>

void			data_setup(t_master *master)
{
	setup_keys(&master->window.keys);
	setup_window(&master->window);
	setup_camera(master->scene->camera, master->window.width,
		master->window.height);
	setup_matrices(master->scene);
	setup_objects(&master->scene->objects[0]);
}
