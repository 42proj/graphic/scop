/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup_world.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 12:33:38 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 11:03:22 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <scop.h>
#include <matrix.h>
#include <matrix_dispatcher.h>

void			setup_camera(t_camera *camera, int width, int height)
{
	camera->fov = deg_to_radians(90);
	camera->aspect_ratio = width / (float)height;
	camera->near = 0.1;
	camera->far = 1000;
}

void			setup_matrices(t_scene *scene)
{
	scene->projection_matrix = new_matrix(4, 4, "identity");
	update_projection_matrix(scene->projection_matrix, scene->camera[0]);
	scene->view_matrix = new_matrix(4, 4, "identity");
	scene->rotation_matrix = new_matrix(4, 4, "identity");
	scene->translation_matrix = new_matrix(4, 4, "identity");
	translate_matrix(&scene->translation_matrix, new_vector(0, 0, -3.1));
	scene->scaling_matrix = new_matrix(4, 4, "identity");
}

void			setup_objects(t_object *objects)
{
	objects->vertices = NULL;
	objects->num_vertices = 0;
	objects->elements = NULL;
	objects->num_elements = 0;
	objects->num_faces = 0;
}
