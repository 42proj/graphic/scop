#version 410 core

in  vec4 color;
in  vec2 TexCoord;

uniform sampler2D ourTexture;

out vec4 FragColor;

void main()
{
    vec4 face_color;
    if (gl_PrimitiveID % 2 == 0)
        face_color = vec4(0.2, 0.2, 0.2, 1);
    else
        face_color = vec4(0.7, 0.7, 0.7, 1);
    FragColor = face_color;
} 