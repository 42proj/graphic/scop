/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix_dispatcher.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 12:23:59 by svilau            #+#    #+#             */
/*   Updated: 2018/10/26 12:24:59 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <scop.h>

void			send_view_matrix(t_shader *shaders, float *view_matrix)
{
	int matrix_location;

	matrix_location = glGetUniformLocation(shaders->program, "viewMatrix");
	glUniformMatrix4fv(matrix_location, 1, GL_FALSE, view_matrix);
}

void			send_translation_matrix(t_shader *shaders, float *model_matrix)
{
	int matrix_location;

	matrix_location = glGetUniformLocation(shaders->program,
		"translationMatrix");
	glUniformMatrix4fv(matrix_location, 1, GL_FALSE, model_matrix);
}

void			send_rotation_matrix(t_shader *shaders, float *rotation_matrix)
{
	int matrix_location;

	matrix_location = glGetUniformLocation(shaders->program, "rotationMatrix");
	glUniformMatrix4fv(matrix_location, 1, GL_FALSE, rotation_matrix);
}

void			send_scaling_matrix(t_shader *shaders, float *scaling_matrix)
{
	int matrix_location;

	matrix_location = glGetUniformLocation(shaders->program, "scalingMatrix");
	glUniformMatrix4fv(matrix_location, 1, GL_FALSE, scaling_matrix);
}

void			send_projection_matrix(t_shader *shaders,
										float *projection_matrix)
{
	int matrix_location;

	matrix_location = glGetUniformLocation(shaders->program,
		"projectionMatrix");
	glUniformMatrix4fv(matrix_location, 1, GL_FALSE, projection_matrix);
}
