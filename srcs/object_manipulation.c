/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   object_manipulation.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 12:08:47 by svilau            #+#    #+#             */
/*   Updated: 2018/11/08 12:10:30 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <vectors.h>
#include <scop.h>

t_vec3d			get_box_min(t_object *object)
{
	t_vec3d			min;
	unsigned int	i;

	i = 0;
	if (object->num_vertices > 0)
	{
		min = new_vector(object->vertices[0].pos.x, object->vertices[0].pos.y,
							object->vertices[0].pos.z);
	}
	while (i < object->num_vertices)
	{
		if (object->vertices[i].pos.x < min.x)
			min.x = object->vertices[i].pos.x;
		if (object->vertices[i].pos.y < min.y)
			min.y = object->vertices[i].pos.y;
		if (object->vertices[i].pos.z < min.z)
			min.z = object->vertices[i].pos.z;
		i++;
	}
	return (min);
}

t_vec3d			get_box_max(t_object *object)
{
	t_vec3d			max;
	unsigned int	i;

	i = 0;
	if (object->num_vertices > 0)
	{
		max = new_vector(object->vertices[0].pos.x, object->vertices[0].pos.y,
							object->vertices[0].pos.z);
	}
	while (i < object->num_vertices)
	{
		if (object->vertices[i].pos.x > max.x)
			max.x = object->vertices[i].pos.x;
		if (object->vertices[i].pos.y > max.y)
			max.y = object->vertices[i].pos.y;
		if (object->vertices[i].pos.z > max.z)
			max.z = object->vertices[i].pos.z;
		i++;
	}
	return (max);
}

t_vec3d			get_center_object(t_object *object)
{
	t_vec3d			center_point;
	t_vec3d			max;
	t_vec3d			min;

	min = get_box_min(object);
	max = get_box_max(object);
	center_point = vector_divide(vector_add(min, max), 2);
	return (center_point);
}

void			center_object(t_scene *scene, t_object *object)
{
	t_vec3d			centering_vector;
	unsigned int	i;

	(void)scene;
	centering_vector = vector_scalar(get_center_object(object), -1);
	i = 0;
	while (i < object->num_vertices)
	{
		vector_translate_vector(&object->vertices[i].pos, centering_vector);
		i++;
	}
}
