/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_obj_useful.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 12:09:08 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 12:16:06 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <display.h>
#include <libft.h>
#include <stdio.h>

GLuint			get_number_words(char *line)
{
	int		i;
	GLuint	word_count;

	word_count = 0;
	i = 0;
	while (line[i] != '\0')
	{
		while (line[i] != ' ' && line[i] != '\0')
			i++;
		while (line[i] == ' ' && line[i] != '\0')
			i++;
		word_count += 1;
	}
	return (word_count);
}

void			count_data(int fd, GLuint *num_vertices, GLuint *num_elements,
							GLuint *num_faces)
{
	char	*line;

	while ((get_next_line(fd, &line)) == 1)
	{
		if (line[0] == 'v')
			*num_vertices += 1;
		if (line[0] == 'f')
		{
			*num_faces += 1;
			*num_elements += (get_number_words(line) - 1);
		}
		free(line);
	}
	free(line);
}
