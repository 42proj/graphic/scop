/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_obj2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 12:31:14 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 12:24:53 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <scop.h>
#include <libft.h>
#include <stdio.h>

void			get_coordinate(double *coordinate, char *data, int *i)
{
	char	*coordinate_char;

	coordinate_char = NULL;
	while (data[*i] != ' ' && data[*i] != '\0')
	{
		append_char(&coordinate_char, data[*i]);
		(*i) += 1;
	}
	*coordinate = ft_atoi_double(coordinate_char);
	free(coordinate_char);
	(*i) += 1;
}

void			get_element(GLuint *element, char *data, int *i)
{
	char	*element_char;

	element_char = NULL;
	while (data[*i] != ' ' && data[*i] != '\0')
	{
		append_char(&element_char, data[*i]);
		(*i) += 1;
	}
	(*element) = ft_atoi(element_char) - 1;
	free(element_char);
	while (data[*i] == ' ' && data[*i] != '\0')
		(*i) += 1;
}

void			load_vertex(t_object *object, char *data, int *i, int *v_index)
{
	t_rgb vertex_color;

	(*i) += 2;
	get_coordinate(&(object->vertices[*v_index].pos.x), data, i);
	get_coordinate(&(object->vertices[*v_index].pos.y), data, i);
	get_coordinate(&(object->vertices[*v_index].pos.z), data, i);
	vertex_color = hsv_to_rgb_int(rand_in_range(0, 360), 1, 1);
	object->vertices[*v_index].col.x = vertex_color.r / (float)255;
	object->vertices[*v_index].col.y = vertex_color.g / (float)255;
	object->vertices[*v_index].col.z = vertex_color.b / (float)255;
	object->vertices[*v_index].tex.x = rand_in_range(0, 1);
	object->vertices[*v_index].tex.y = rand_in_range(0, 1);
	(*v_index)++;
}

void			load_element(t_object *object, char *data, int *i, int *e_index)
{
	(*i) += 2;
	while (data[(*i)] != '\0')
	{
		get_element(&(object->elements[*e_index]), data, i);
		(*e_index)++;
	}
}
