/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_obj.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 12:05:30 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 12:25:10 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <fcntl.h>
#include <display.h>
#include <meshes.h>
#include <errors.h>
#include <parser.h>
#include <scop.h>

void			parse_line(char *data, t_object *object, int *e_index,
					int *v_index)
{
	int	i;

	i = 0;
	if (data[i] == 'v' && data[i + 1] == ' ')
		load_vertex(object, data, &i, v_index);
	if (data[i] == 'f' && data[i + 1] == ' ')
	{
		load_element(object, data, &i, e_index);
		object->elements[*e_index] = PRIMITIVE_RESTART;
		(*e_index)++;
	}
}

void			load_object(t_object *object, char *filename)
{
	int				v_index;
	int				e_index;
	char			*data;
	int				fd;

	v_index = 0;
	e_index = 0;
	if ((fd = open(filename, O_RDONLY)) < 0)
		error_handler("open");
	count_data(fd, &(object->num_vertices), &(object->num_elements));
	if (!object->num_vertices || !object->num_elements)
		error_handler("Empty object");
	lseek(fd, 0, SEEK_SET);
	if (!(object->vertices = malloc(sizeof(t_vertex) * object->num_vertices)))
		error_handler("malloc");
	if (!(object->elements = malloc(sizeof(GLuint) * (object->num_elements +
			object->num_faces))))
		error_handler("malloc");
	while ((get_next_line(fd, &data)) == 1)
	{
		parse_line(data, object, &e_index, &v_index);
		free(data);
	}
	free(data);
	close(fd);
}
