/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recurring_events.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 14:14:12 by svilau            #+#    #+#             */
/*   Updated: 2018/11/13 14:31:12 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <scop.h>
#include <matrix.h>

void			recurring_events1(t_window *window, t_scene *scene,
					t_shader *shaders)
{
	(void)scene;
	if (window->keys.t == 1 && window->keys.y == 0)
	{
		load_shaders(shaders, "./srcs/shaders/vertex.vtx",
				"./srcs/shaders/fragment_no_texture.frg");
	}
	else if (window->keys.y == 1 && window->keys.t == 0)
	{
		load_shaders(shaders, "./srcs/shaders/vertex.vtx",
			"./srcs/shaders/fragment_smooth_faces.frg");
	}
	else
	{
		load_shaders(shaders, "./srcs/shaders/vertex.vtx",
			"./srcs/shaders/fragment.frg");
	}
}

void			recurring_events2(t_window *window, t_scene *scene,
					t_shader *shaders)
{
	(void)shaders;
	if (window->keys.r == 1)
	{
		rotate_matrix(&scene->rotation_matrix, new_vector(0, 1, 0),
			deg_to_radians(0.5));
	}
	if (window->keys.space == 1)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}

void			handle_recurring_events(t_window *window, t_scene *scene,
					t_shader *shaders)
{
	recurring_events1(window, scene, shaders);
	recurring_events2(window, scene, shaders);
}
