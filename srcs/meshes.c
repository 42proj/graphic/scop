/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   meshes.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/23 11:50:10 by svilau            #+#    #+#             */
/*   Updated: 2018/11/13 16:59:34 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <scop.h>
#include <vectors.h>
#include <meshes.h>
#include <stdio.h>
#include <stdlib.h>

t_vertex	vertex(t_vec3d pos)
{
	t_vertex vertex;

	vertex.pos = pos;
	return (vertex);
}

void		mesh(t_mesh *mesh, t_object object)
{
	mesh->draw_count = object.num_elements + object.num_faces;
	glGenVertexArrays(1, &(mesh->vertex_array_object));
	glBindVertexArray(mesh->vertex_array_object);
	glGenBuffers(NUM_BUFFERS, &(mesh->vertex_array_buffers));
	glBindBuffer(GL_ARRAY_BUFFER, mesh->vertex_array_buffers);
	glBufferData(GL_ARRAY_BUFFER, object.num_vertices *
		sizeof(object.vertices[0]), object.vertices, GL_STATIC_DRAW);
	glGenBuffers(NUM_BUFFERS, &(mesh->element_buffer_object));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->element_buffer_object);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (object.num_elements +
		object.num_faces) * sizeof(object.elements[0]), object.elements,
		GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, 8 * sizeof(double), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_DOUBLE, GL_FALSE, 8 * sizeof(double),
		(void*)(3 * sizeof(double)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_DOUBLE, GL_FALSE, 8 * sizeof(double),
		(void*)(6 * sizeof(double)));
}

void		delete_mesh(t_mesh *mesh)
{
	glDeleteVertexArrays(0, &(mesh->vertex_array_object));
}

void		draw(t_mesh *mesh)
{
	glBindVertexArray(mesh->vertex_array_object);
	glPrimitiveRestartIndex(PRIMITIVE_RESTART);
	glDrawElements(GL_TRIANGLE_FAN, mesh->draw_count, GL_UNSIGNED_INT,
					(void*)0);
	glBindVertexArray(0);
}
