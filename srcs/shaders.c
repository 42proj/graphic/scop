/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shaders.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 10:03:00 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 12:49:47 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <scop.h>
#include <stdbool.h>
#include <stdlib.h>
#include <libft.h>
#include <files.h>
#include <errors.h>
#include <stdio.h>

/*
** printf("%s : %s\n", errorMessage, error);
*/

void	check_shader_error(GLuint shader, GLuint flag, bool is_program,
						const char *error_message)
{
	GLint	success;
	GLchar	error[1024];

	success = 0;
	if (is_program)
		glGetProgramiv(shader, flag, &success);
	else
		glGetShaderiv(shader, flag, &success);
	if (success == GL_FALSE)
	{
		if (is_program)
			glGetProgramInfoLog(shader, sizeof(error), NULL, error);
		else
			glGetShaderInfoLog(shader, sizeof(error), NULL, error);
		printf("%s : %s\n", error_message, error);
	}
}

GLuint	create_shader(char *shader_code, GLenum shader_type)
{
	GLuint			shader;
	const GLchar	*shader_source[1];
	GLint			shader_source_string_length[1];

	shader_source[0] = shader_code;
	shader_source_string_length[0] = ft_strlen(shader_code);
	shader = glCreateShader(shader_type);
	if (shader == 0)
		error_handler("shader");
	glShaderSource(shader, 1, shader_source, shader_source_string_length);
	glCompileShader(shader);
	check_shader_error(shader, GL_COMPILE_STATUS, false,
		"Error compiling shader!");
	return (shader);
}

void	delete_shaders(t_shader *shaders)
{
	glDetachShader(shaders->program, shaders->vtx);
	glDetachShader(shaders->program, shaders->frg);
	glDeleteShader(shaders->vtx);
	glDeleteShader(shaders->frg);
	glDeleteProgram(shaders->program);
	return ;
}

void	load_shaders(t_shader *shaders, char *vtx_filename, char *frg_filename)
{
	char *tmp;

	shaders->program = glCreateProgram();
	tmp = load_file(vtx_filename);
	shaders->vtx = create_shader(tmp, GL_VERTEX_SHADER);
	free(tmp);
	tmp = load_file(frg_filename);
	shaders->frg = create_shader(tmp, GL_FRAGMENT_SHADER);
	free(tmp);
	glAttachShader(shaders->program, shaders->vtx);
	glAttachShader(shaders->program, shaders->frg);
	glBindAttribLocation(shaders->program, 0, "position");
	glLinkProgram(shaders->program);
	check_shader_error(shaders->program, GL_LINK_STATUS, true,
		"Error linking shader program");
	glValidateProgram(shaders->program);
	check_shader_error(shaders->program, GL_LINK_STATUS, true,
		"Invalid shader program");
	glDeleteShader(shaders->vtx);
	glDeleteShader(shaders->frg);
	return ;
}

void	shader_bind(t_shader shaders)
{
	glUseProgram(shaders.program);
}
