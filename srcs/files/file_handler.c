/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_handler.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 11:13:37 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 12:46:32 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <libft.h>
#include <errors.h>

char	*load_file(char *filename)
{
	char	*text;
	char	*line;
	int		fd;

	text = NULL;
	if ((fd = open(filename, O_RDONLY)) < 0)
		error_handler("open");
	append(&text, "\0");
	while ((get_next_line(fd, &line)) == 1)
	{
		append(&text, line);
		append(&text, "\n");
		free(line);
	}
	free(line);
	close(fd);
	return (text);
}
