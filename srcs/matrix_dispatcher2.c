/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix_dispatcher2.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 13:14:06 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 10:42:10 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <camera.h>

void			update_projection_matrix(float *projection_matrix,
	t_camera camera)
{
	float top;
	float bottom;
	float right;
	float left;

	top = tan(camera.fov / 2) * camera.near;
	bottom = (-1) * top;
	left = bottom * camera.aspect_ratio;
	right = top * camera.aspect_ratio;
	projection_matrix[0] = (2 * camera.near) / (right - left);
	projection_matrix[2] = (right + left) / (right - left);
	projection_matrix[5] = (2 * camera.near) / (top - bottom);
	projection_matrix[6] = (top + bottom) / (top - bottom);
	projection_matrix[10] = (-1) * ((camera.far + camera.near) /
		(camera.far - camera.near));
	projection_matrix[14] = (-1) * ((2 * camera.far * camera.near) /
		(camera.far - camera.near));
	projection_matrix[11] = -1;
}
