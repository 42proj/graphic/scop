/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 09:36:08 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 10:37:34 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <display.h>
#include <libft.h>
#include <stdio.h>
#include <errors.h>

/*
** void	print_available_extensions(void)
** {
** 	GLint n;
** 	GLint i;
**
** 	glGetIntegerv(GL_NUM_EXTENSIONS, &n);
** 	for(i = 0; i < n; i++)
** 	{
**		const char *extension = (const char*)glGetStringi(GL_EXTENSIONS, i);
**		printf("Extension #%d: %s\n", i, extension);
** 	}
** }
*/

void	clear_window(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
	glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT);
}

void	framebuffer_size_callback(int width, int height)
{
	glViewport(0, 0, width, height);
}

int		init_window(t_window *window)
{
	if (!glfwInit())
		error_handler("GLFW initialisation failed");
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	window->id = glfwCreateWindow(window->width, window->height, window->title,
		NULL, NULL);
	if (!(window->id))
	{
		glfwTerminate();
		error_handler("WINDOW initialisation failed");
		return (-1);
	}
	glfwMakeContextCurrent(window->id);
	glfwSetWindowUserPointer(window->id, window);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_PRIMITIVE_RESTART);
	glewExperimental = GL_TRUE;
	if (glewInit())
		error_handler("GLEW initialisation failed");
	return (1);
}

void	setup_window_title(t_window *window)
{
	append(&window->title, window->program_name);
	append(&window->title, window->version_major);
	append(&window->title, ".");
	append(&window->title, window->version_minor);
	append(&window->title, ".");
	append(&window->title, window->version_patch);
}

void	setup_window(t_window *window)
{
	window->id = NULL;
	window->height = 1280;
	window->width = 1280;
	window->version_major = "1";
	window->version_minor = "0";
	window->version_patch = "0";
	window->program_name = "Scop ";
	window->title = NULL;
	setup_window_title(window);
}
