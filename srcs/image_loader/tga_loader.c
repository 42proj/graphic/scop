/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tga_loader.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 12:41:40 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 12:32:58 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <errors.h>
#include <libft.h>
#include <string.h>
#include <tga_loader.h>

void			get_header(t_tga_header **tga_header, unsigned char *data)
{
	*tga_header = (t_tga_header*)(data);
}

void			get_image_data(t_tga_image *image, unsigned char *data)
{
	int		size;
	int		image_data_offset;

	size = (int)image->header->width * (int)image->header->height *
			((int)(image->header->bits_per_pixel / 8));
	if (!(image->image_data = ft_memalloc(size + 1)))
		error_handler("Memorry allocation for pixel buffer");
	image_data_offset = 18 + (int)image->header->id_length;
	memcpy(image->image_data, data + image_data_offset, size);
}

/*
** printf("ID Length: %d\n", image.header->id_length);
** printf("Color map type: %d\n", image.header->color_map_type);
** printf("Image type: %d\n", image.header->data_type_code);
** printf("First entry index: %d\n", image.header->color_map_origin);
** printf("Color map length: %d\n", image.header->color_map_length);
** printf("Color map depth: %d\n", image.header->color_map_depth);
** printf("X Origin: %d\n", image.header->x_origin);
** printf("Y Origin: %d\n", image.header->y_origin);
** printf("Width: %d\n", image.header->width);
** printf("Height: %d\n", image.header->height);
** printf("Bits per pixel: %d\n", image.header->bits_per_pixel);
*/

t_tga_image		load_tga(char *filename)
{
	int				fd;
	t_tga_image		image;
	struct stat		buf;
	unsigned char	*data;

	if (stat(filename, &buf) == -1)
		error_handler("Getting file stat");
	if (!(fd = open(filename, O_RDONLY)))
		error_handler("Opening tga texture file");
	data = mmap(NULL, buf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (data == MAP_FAILED)
		error_handler("Mmap failed");
	get_header(&image.header, data);
	get_image_data(&image, data);
	return (image);
}
