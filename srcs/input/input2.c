/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 10:24:53 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 13:01:53 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <scop.h>
#include <matrix.h>
#include <errors.h>

void	automatic_rotation_handler(GLFWwindow *window, int key, int action)
{
	t_window *tmp_window;

	tmp_window = glfwGetWindowUserPointer(window);
	if (key == GLFW_KEY_R && action == GLFW_RELEASE)
	{
		if (tmp_window->keys.r == 1)
			tmp_window->keys.r = 0;
		else if (tmp_window->keys.r == 0)
			tmp_window->keys.r = 1;
		else
			error_handler("T key value different than 0 or 1");
	}
}

void	texture_handler(GLFWwindow *window, int key, int action)
{
	t_window *tmp_window;

	tmp_window = glfwGetWindowUserPointer(window);
	if (key == GLFW_KEY_T && action == GLFW_PRESS)
	{
		if (tmp_window->keys.t == 0)
			tmp_window->keys.t = 1;
		else if (tmp_window->keys.t == 1)
			tmp_window->keys.t = 0;
		else
			error_handler("T key value different than 0 or 1");
		if (tmp_window->keys.y == 1)
			tmp_window->keys.y = 0;
	}
	if (key == GLFW_KEY_Y && action == GLFW_PRESS)
	{
		if (tmp_window->keys.y == 0)
			tmp_window->keys.y = 1;
		else if (tmp_window->keys.y == 1)
			tmp_window->keys.y = 0;
		else
			error_handler("Y key value different than 0 or 1");
		if (tmp_window->keys.t == 1)
			tmp_window->keys.t = 0;
	}
}

void	wireframe_handler(GLFWwindow *window, int key, int action)
{
	t_window *tmp_window;

	tmp_window = glfwGetWindowUserPointer(window);
	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
	{
		if (tmp_window->keys.space == 0)
		{
			tmp_window->keys.space = 1;
		}
		else if (tmp_window->keys.space == 1)
		{
			tmp_window->keys.space = 0;
		}
		else
			error_handler("W key value different than 0 or 1");
	}
}

void	toggle_actions_handler(GLFWwindow *window, int key, int scancode,
			int action, int mods)
{
	(void)scancode;
	(void)mods;
	wireframe_handler(window, key, action);
	automatic_rotation_handler(window, key, action);
	texture_handler(window, key, action);
}
