/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/16 11:09:00 by svilau            #+#    #+#             */
/*   Updated: 2018/11/12 18:13:54 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <scop.h>
#include <display.h>
#include <effects.h>
#include <matrix.h>
#include <input.h>
#include <errors.h>

void	translation_handler(t_window *window, t_scene *scene)
{
	if (glfwGetKey(window->id, GLFW_KEY_E) == GLFW_PRESS)
		translate_matrix(&scene->translation_matrix, new_vector(0, 0, 0.1));
	if (glfwGetKey(window->id, GLFW_KEY_Q) == GLFW_PRESS)
		translate_matrix(&scene->translation_matrix, new_vector(0, 0, -0.1));
	if (glfwGetKey(window->id, GLFW_KEY_W) == GLFW_PRESS)
		translate_matrix(&scene->translation_matrix, new_vector(0, 0.1, 0));
	if (glfwGetKey(window->id, GLFW_KEY_S) == GLFW_PRESS)
		translate_matrix(&scene->translation_matrix, new_vector(0, -0.1, 0));
	if (glfwGetKey(window->id, GLFW_KEY_A) == GLFW_PRESS)
		translate_matrix(&scene->translation_matrix, new_vector(-0.1, 0, 0));
	if (glfwGetKey(window->id, GLFW_KEY_D) == GLFW_PRESS)
		translate_matrix(&scene->translation_matrix, new_vector(0.1, 0, 0));
}

void	rotation_handler(t_window *window, t_scene *scene)
{
	if (glfwGetKey(window->id, GLFW_KEY_RIGHT) == GLFW_PRESS)
		rotate_matrix(&scene->rotation_matrix, new_vector(0, 1, 0),
			deg_to_radians(10));
	if (glfwGetKey(window->id, GLFW_KEY_LEFT) == GLFW_PRESS)
		rotate_matrix(&scene->rotation_matrix, new_vector(0, 1, 0),
			deg_to_radians(-10));
	if (glfwGetKey(window->id, GLFW_KEY_UP) == GLFW_PRESS)
		rotate_matrix(&scene->rotation_matrix, new_vector(1, 0, 0),
			deg_to_radians(10));
	if (glfwGetKey(window->id, GLFW_KEY_DOWN) == GLFW_PRESS)
		rotate_matrix(&scene->rotation_matrix, new_vector(1, 0, 0),
			deg_to_radians(-10));
	if (glfwGetKey(window->id, GLFW_KEY_LEFT_BRACKET) == GLFW_PRESS)
		rotate_matrix(&scene->rotation_matrix, new_vector(0, 0, 1),
			deg_to_radians(-10));
	if (glfwGetKey(window->id, GLFW_KEY_RIGHT_BRACKET) == GLFW_PRESS)
		rotate_matrix(&scene->rotation_matrix, new_vector(0, 0, 1),
			deg_to_radians(10));
}

void	scale_handler(t_window *window, t_scene *scene)
{
	if (glfwGetKey(window->id, GLFW_KEY_KP_ADD) == GLFW_PRESS)
		scale_matrix(&scene->scaling_matrix, 1.01, 1.01, 1.01);
	if (glfwGetKey(window->id, GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS)
		scale_matrix(&scene->scaling_matrix, 0.99, 0.99, 0.99);
}

void	handle_input(t_window *window, t_scene *scene)
{
	if (glfwGetKey(window->id, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window->id, GL_TRUE);
	translation_handler(window, scene);
	rotation_handler(window, scene);
	scale_handler(window, scene);
	glfwSetKeyCallback(window->id, toggle_actions_handler);
}
