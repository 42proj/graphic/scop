/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 08:48:06 by aastefian         #+#    #+#             */
/*   Updated: 2018/11/15 12:55:29 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <scop.h>
#include <matrix.h>
#include <stdlib.h>
#include <stdio.h>
#include <libft.h>
#include <errors.h>

void	init_matrix(float *matrix, int m_width, int m_height, char *init)
{
	int i;
	int size;

	i = 0;
	(void)init;
	size = m_height * m_width;
	while (i < size)
	{
		if ((i % (m_width + 1)) == 0 && ft_strcmp(init, "identity") == 0)
			matrix[i] = 1;
		else
			matrix[i] = 0;
		i++;
	}
}

float	*translate_matrix(float **matrix, t_vec3d translation_vector)
{
	float *matrix_tmp;
	float *matrix2_tmp;

	matrix_tmp = new_matrix(4, 4, "identity");
	matrix_tmp[12] = translation_vector.x;
	matrix_tmp[13] = translation_vector.y;
	matrix_tmp[14] = translation_vector.z;
	matrix2_tmp = matrix_multiply(*matrix, matrix_tmp, 4, 4);
	free(*matrix);
	free(matrix_tmp);
	*matrix = matrix2_tmp;
	return (*matrix);
}

float	*scale_matrix(float **matrix, float x, float y, float z)
{
	float *matrix_tmp;
	float *matrix2_tmp;

	matrix_tmp = new_matrix(4, 4, "identity");
	matrix_tmp[0] = x;
	matrix_tmp[5] = y;
	matrix_tmp[10] = z;
	matrix2_tmp = matrix_multiply(*matrix, matrix_tmp, 4, 4);
	free(*matrix);
	free(matrix_tmp);
	*matrix = matrix2_tmp;
	return (*matrix);
}

float	*rotate_matrix(float **matrix, t_vec3d axis, float angle)
{
	float *matrix_tmp;
	float *matrix2_tmp;
	float cos_angle;
	float sin_angle;

	cos_angle = cos(angle);
	sin_angle = sin(angle);
	matrix_tmp = new_matrix(4, 4, "identity");
	matrix_tmp[0] = cos_angle + pow(axis.x, 2) * (1 - cos_angle);
	matrix_tmp[1] = axis.x * axis.y * (1 - cos_angle) - axis.z * sin_angle;
	matrix_tmp[2] = axis.x * axis.z * (1 - cos_angle) + axis.y * sin_angle;
	matrix_tmp[4] = axis.y * axis.x * (1 - cos_angle) + axis.z * sin_angle;
	matrix_tmp[5] = cos_angle + pow(axis.y, 2) * (1 - cos_angle);
	matrix_tmp[6] = axis.y * axis.z * (1 - cos_angle) - axis.x * sin_angle;
	matrix_tmp[8] = axis.z * axis.x * (1 - cos_angle) - axis.y * sin_angle;
	matrix_tmp[9] = axis.z * axis.y * (1 - cos_angle) + axis.x * sin_angle;
	matrix_tmp[10] = cos_angle + pow(axis.z, 2) * (1 - cos_angle);
	matrix2_tmp = matrix_multiply(*matrix, matrix_tmp, 4, 4);
	free(*matrix);
	free(matrix_tmp);
	*matrix = matrix2_tmp;
	return (*matrix);
}

float	*new_matrix(int m_width, int m_height, char *init)
{
	float *matrix;

	if (!(matrix = malloc(sizeof(float) * m_width * m_height)))
		error_handler("Malloc: new_matrix()");
	init_matrix(matrix, m_width, m_height, init);
	return (matrix);
}
