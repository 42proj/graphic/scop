/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 12:34:30 by svilau            #+#    #+#             */
/*   Updated: 2018/11/08 16:27:28 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <errors.h>

float	*matrix_multiply(float *matrix1, float *matrix2, int m_width,
							int m_height)
{
	float	*result;
	int		n;
	int		i;
	int		j;
	int		x;

	if (!(result = malloc(sizeof(float) * (m_width * m_height))))
		error_handler("Malloc: matrix_multiply()");
	i = 0;
	n = 0;
	while (n < (m_width * m_height))
	{
		x = n % m_width;
		j = 0;
		result[n] = 0;
		while (j < m_width)
		{
			result[n] += matrix1[i * m_width + j] * matrix2[j * m_width + x];
			j++;
		}
		n++;
		if (n % m_width == 0)
			i++;
	}
	return (result);
}
