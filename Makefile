# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: svilau <svilau@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/17 16:21:29 by svilau            #+#    #+#              #
#    Updated: 2018/11/14 14:43:29 by svilau           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = scop

MAKE = make
MAKE_FLAGS = --no-print-directory

SRC_DIR = srcs

LIBFT_DIR = ./libft
LIBFT = $(LIBFT_DIR)/libft.a

GLEW = frameworks/glew/lib/libGLEW.2.1.dylib
GLFW3 = frameworks/glfw/lib/libglfw.3.2.dylib
OPENGL = -framework OpenGL

INCLUDE = -I frameworks/glew/include/GL -I frameworks/glfw/include/GLFW -I includes -I libft

CC = gcc -g
CC_FLAGS = -Wall -Werror -Wextra

#OS= $(shell lsb_release -si)

RM = rm -f
RF = rm -rf

###################################################
##--- LIST of Sources                         ---##
###################################################

SRC =	$(SRC_DIR)/main.c \
		$(SRC_DIR)/object_manipulation.c \
		$(SRC_DIR)/display.c \
		$(SRC_DIR)/shaders.c \
		$(SRC_DIR)/meshes.c \
		$(SRC_DIR)/recurring_events.c \
		$(SRC_DIR)/input/input.c \
		$(SRC_DIR)/input/input2.c \
		$(SRC_DIR)/matrix_dispatcher.c \
		$(SRC_DIR)/matrix_dispatcher2.c \
		$(SRC_DIR)/matrix/matrix.c \
		$(SRC_DIR)/matrix/matrix2.c \
		$(SRC_DIR)/vectors/vectors.c \
		$(SRC_DIR)/vectors/vectors2.c \
		$(SRC_DIR)/vectors/rotation.c \
		$(SRC_DIR)/vectors/translation.c \
		$(SRC_DIR)/conversion/angles.c \
		$(SRC_DIR)/error_handling/errors.c \
		$(SRC_DIR)/files/file_handler.c \
		$(SRC_DIR)/image_loader/tga_loader.c \
		$(SRC_DIR)/parser/parser_obj.c \
		$(SRC_DIR)/parser/parser_obj2.c \
		$(SRC_DIR)/parser/parser_obj_useful.c \
		$(SRC_DIR)/setup_functions/setup_dispatcher.c \
		$(SRC_DIR)/setup_functions/setup_keys.c \
		$(SRC_DIR)/setup_functions/setup_world.c \

###################################################
##--- Use Patsubst to find .o from .c         ---##
###################################################

OBJ		 = $(patsubst srcs/%.c, obj/%.o, $(SRC))
.SILENT:

###################################################
##--- Compilation Running                     ---##
###################################################

all: $(NAME)

$(NAME): $(OBJ) $(OBJ_CUDA)
	printf '\033[K\033[32m[✔] %s\n\033[0m' "--Compiling Sources--------"
	@$(MAKE) $(MAKE_FLAGS) -C $(LIBFT_DIR)
	printf '\033[32m[✔] %s\n\033[0m' "--Compiling Libft Library--------"
	if [ ! -d bin ]; then mkdir -p bin; fi
#ifeq ($(OS),Ubuntu)
#	$(CC) $(CC_FLAGS) -o bin/$(NAME) $(OBJ) $(LIBFT) -lm -lglfw -lGL -lGLEW
#else
	$(CC) $(CC_FLAGS) -o bin/$(NAME) $(OBJ) $(LIBFT) $(GLEW) $(GLFW3) $(OPENGL)
#endif
	printf '\033[1;7m'
	printf '\033[33m[✔] %s\n\033[0m' "--DONE--------"

###################################################
##--- Create repertories for objects .c to .c ---##
###################################################

obj/%.o: srcs/%.c
	if [ ! -d obj ]; then mkdir -p obj; fi
	if [ ! -d $(dir $@) ]; then mkdir -p $(dir $@); fi
	$(CC) $(CC_FLAGS) -c $(INCLUDE) $< -o $@
	printf '\033[K\033[0m[✔] %s\n\033[0m\033[1A' "$<"

###################################################
##--- Clean only objects .o                      ##
###################################################

clean:
	printf '\033[1;7m'
	printf '\033[31m[✔] %s\n\033[0m' "--Cleaning Library--------"
	@$(MAKE) $(MAKE_FLAGS) fclean -C $(LIBFT_DIR)
	printf '\033[1;7m'
	printf '\033[31m[✔] %s\n\033[0m' "--Cleaning Output Files--------"
	@$(RM) $(OBJ)

###################################################
##--- Clean ALl                                  ##
###################################################

fclean: clean
	printf '\033[1;7m'
	printf '\033[31m[✔] %s\n\033[0m' "--Cleaning Executable and All Object--------"
	@$(RM) $(NAME)
	@$(RF) obj bin

re: fclean all
