/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 14:29:41 by svilau            #+#    #+#             */
/*   Updated: 2018/10/25 14:30:10 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATRIX_H
# define MATRIX_H

# include <vectors.h>

float	*new_matrix(int m_width, int m_height, char *init);
void	init_matrix(float *matrix, int m_width, int m_height, char *init);
float	*matrix_multiply(float *matrix1, float *matrix2, int m_width,
			int m_height);
float	*rotate_matrix(float **matrix, t_vec3d axis, float angle);
float	*scale_matrix(float **matrix, float x, float y, float z);
float	*translate_matrix(float **matrix, t_vec3d translation_vector);

#endif
