/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix_dispatcher.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 12:25:40 by svilau            #+#    #+#             */
/*   Updated: 2018/10/26 13:16:05 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATRIX_DISPATCHER_H
# define MATRIX_DISPATCHER_H

# include <scop.h>

void	send_view_matrix(t_shader *shaders, float *view_matrix);
void	send_translation_matrix(t_shader *shaders, float *model_matrix);
void	send_rotation_matrix(t_shader *shaders, float *rotation_matrix);
void	send_scaling_matrix(t_shader *shaders, float *scaling_matrix);
void	send_projection_matrix(t_shader *shaders, float *projection_matrix);
void	update_projection_matrix(float *projection_matrix, t_camera camera);

#endif
