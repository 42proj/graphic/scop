/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tga_loader.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 14:01:48 by svilau            #+#    #+#             */
/*   Updated: 2018/11/12 16:56:08 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TGA_LOADER_H
# define TGA_LOADER_H
# pragma pack(push, 1)

typedef struct		s_tga_header
{
	char		id_length;
	char		color_map_type;
	char		data_type_code;
	short int	color_map_origin;
	short int	color_map_length;
	char		color_map_depth;
	short int	x_origin;
	short int	y_origin;
	short		width;
	short		height;
	char		bits_per_pixel;
	char		image_descriptor;

}					t_tga_header;
# pragma pack(pop)

typedef struct		s_tga_image
{
	t_tga_header	*header;
	unsigned char	*image_data;
}					t_tga_image;

t_tga_image			load_tga(char *filename);

#endif
