/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 14:23:00 by svilau            #+#    #+#             */
/*   Updated: 2018/10/26 14:38:59 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CAMERA_H
# define CAMERA_H

# include <vectors.h>

typedef struct		s_camera
{
	t_vec3d			look_at;
	t_vec3d			pos;
	float			fov;
	float			aspect_ratio;
	float			near;
	float			far;
}					t_camera;

#endif
