/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 14:29:20 by svilau            #+#    #+#             */
/*   Updated: 2018/11/08 12:22:48 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INPUT_H
# define INPUT_H

# include <scop.h>

void	toggle_actions_handler(GLFWwindow *window, int key, int scancode,
			int action, int mods);
void	handle_input(t_window *window, t_scene *scene);

#endif
