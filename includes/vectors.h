/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectors.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/20 10:50:12 by svilau            #+#    #+#             */
/*   Updated: 2018/11/08 10:19:18 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTORS_H
# define VECTORS_H

# include <math.h>
# include <conversion.h>

typedef struct		s_vec2d
{
	double	x;
	double	y;
}					t_vec2d;

typedef struct		s_vec3d

{
	double	x;
	double	y;
	double	z;
}					t_vec3d;

void				rotate(t_vec3d *point, double degrees, char axis);
void				translate(t_vec3d *point, double x,
						double y, double z);
void				vector_translate_vector(t_vec3d *point,
						t_vec3d translation_vector);
t_vec3d				new_vector(double x, double y, double z);
t_vec3d				vector_copy(t_vec3d cpy);
t_vec3d				vector_add(t_vec3d vect1, t_vec3d vect2);
t_vec3d				vector_substract(t_vec3d vect1, t_vec3d vect2);
t_vec3d				vector_cross(t_vec3d vect1, t_vec3d vect2);
t_vec3d				vector_scalar(t_vec3d vect1, double scalar);
t_vec3d				vector_divide(t_vec3d vect1, double scalar);
t_vec3d				vector_calculate(t_vec3d vect1, t_vec3d vect2);
t_vec3d				vector_normalize(t_vec3d vect1);
double				vector_dot(t_vec3d vect1, t_vec3d vect2);
double				vector_length(t_vec3d vect1);
double				vector_magnitude(t_vec3d vect1);
void				vector_rot_y(t_vec3d *z, t_vec3d *x, double angle);
void				vector_rot_x(t_vec3d *y, t_vec3d *z, double angle);
void				vector_rot_z(t_vec3d *x, t_vec3d *y, double angle);

#endif
