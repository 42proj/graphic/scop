/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   object_manipulation.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 12:10:47 by svilau            #+#    #+#             */
/*   Updated: 2018/11/08 12:22:20 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OBJECT_MANIPULATION_H
# define OBJECT_MANIPULATION_H

# include <vectors.h>
# include <scop.h>

t_vec3d			get_box_min(t_object *object);
t_vec3d			get_box_max(t_object *object);
t_vec3d			get_center_object(t_object *object);
void			center_object(t_scene *scene, t_object *object);

#endif
