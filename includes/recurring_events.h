/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recurring_events.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 14:15:12 by svilau            #+#    #+#             */
/*   Updated: 2018/11/13 14:33:45 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RECURRING_EVENTS_H
# define RECURRING_EVENTS_H

# include <scop.h>

void			handle_recurring_events(t_window *window, t_scene *scene,
					t_shader *shaders);

#endif
