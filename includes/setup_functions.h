/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup_functions.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 12:48:53 by svilau            #+#    #+#             */
/*   Updated: 2018/10/26 14:38:11 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SETUP_FUNCTIONS_H
# define SETUP_FUNCTIONS_H

# include <scop.h>

void			data_setup(t_master *master);
void			setup_keys(t_keys *keys);
void			setup_camera(t_camera *camera, int width, int height);
void			setup_matrices(t_scene *scene);
void			setup_objects(t_object *objects);

#endif
