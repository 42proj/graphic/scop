/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 12:11:34 by svilau            #+#    #+#             */
/*   Updated: 2018/11/08 16:31:09 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

void	count_data(int fd, GLuint *num_vertices, GLuint *num_elements);
void	load_object(t_object *object, char *filename);
void	get_coordinate(double *coordinate, char *data, int *i);
void	get_element(GLuint *element, char *data, int *i);
void	load_vertex(t_object *object, char *data, int *i, int *v_index);
void	load_element(t_object *object, char *data, int *i, int *e_index);

#endif
