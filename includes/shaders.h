/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shaders.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 14:32:09 by svilau            #+#    #+#             */
/*   Updated: 2018/10/26 14:38:07 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHADERS_H
# define SHADERS_H

# include <display.h>

typedef struct		s_shader
{
	GLuint			program;
	GLuint			frg;
	GLuint			vtx;

}					t_shader;

void				load_shaders(t_shader *shaders, char *vtx_filename,
									char *frg_filenames);
void				shader_bind(t_shader shaders);

#endif
