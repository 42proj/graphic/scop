/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 14:31:38 by svilau            #+#    #+#             */
/*   Updated: 2018/11/08 14:24:03 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCOP_H
# define SCOP_H
# include <limits.h>
# define PRIMITIVE_RESTART (GLuint)UINT_MAX

# include <camera.h>
# include <shaders.h>
# include <meshes.h>
# include <display.h>

typedef struct		s_scene
{
	t_camera	*camera;
	t_shader	*shader;
	t_object	*objects;
	float		*projection_matrix;
	float		*view_matrix;
	float		*translation_matrix;
	float		*rotation_matrix;
	float		*scaling_matrix;
}					t_scene;

typedef struct		s_master
{
	t_scene		*scene;
	t_window	window;
}					t_master;

#endif
