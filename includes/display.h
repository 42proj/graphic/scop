/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 14:03:16 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 10:22:24 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DISPLAY_H
# define DISPLAY_H

# include <glew.h>

# include <glfw3.h>
# include <input_keys.h>

typedef struct		s_window
{
	GLFWwindow	*id;
	t_keys		keys;
	int			height;
	int			width;
	char		*title;
	char		*program_name;
	char		*version_major;
	char		*version_minor;
	char		*version_patch;
}					t_window;

void				clear_window(float r, float g, float b, float a);
void				framebuffer_size_callback(int width, int height);
int					init_window(t_window *window);
void				setup_window_title(t_window *window);
void				setup_window(t_window *window);

#endif
