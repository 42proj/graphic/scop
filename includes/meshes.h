/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   meshes.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 14:30:52 by svilau            #+#    #+#             */
/*   Updated: 2018/11/06 11:50:58 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MESHES_H
# define MESHES_H

# define NUM_BUFFERS 1
# define POSITION_VB 0

# include <vectors.h>
# include <display.h>

typedef struct		s_vertex
{
	t_vec3d			pos;
	t_vec3d			col;
	t_vec2d			tex;
}					t_vertex;

typedef struct		s_object
{
	t_vertex	*vertices;
	GLuint		num_vertices;
	GLuint		*elements;
	GLuint		num_elements;
	GLuint		num_faces;
}					t_object;

typedef struct		s_mesh
{
	GLuint			vertex_array_object;
	GLuint			vertex_array_buffers;
	GLuint			element_buffer_object;
	GLuint			draw_count;
}					t_mesh;

t_vertex			vertex(t_vec3d pos);
void				mesh(t_mesh *mesh, t_object object);
void				draw(t_mesh *mesh);

#endif
