/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aastefian <aastefian@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/26 10:24:12 by svilau            #+#    #+#             */
/*   Updated: 2017/11/21 12:25:53 by aastefian        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

int		ft_strcmp(char *s1, char *s2)
{
	int				i;
	unsigned int	j;

	i = 0;
	if (s1 == NULL)
		return (ft_strlen(s2) * (-1));
	if (s2 == NULL)
		return (ft_strlen(s1));
	while (s1[i] != '\0' || s2[i] != '\0')
	{
		if (s1[i] != s2[i])
		{
			j = (unsigned char)s1[i] - (unsigned char)s2[i];
			return (j);
		}
		i++;
	}
	return (0);
}
