/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   append_char.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 11:05:19 by svilau            #+#    #+#             */
/*   Updated: 2018/10/26 11:07:03 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "append.h"

void	append_char(char **base, char append)
{
	char	*tmp;
	int		size;

	size = ft_strlen(*base) + 2;
	tmp = malloc(sizeof(char) * size);
	ft_bzero(tmp, size);
	ft_strcpy(tmp, *base);
	tmp[size - 2] = append;
	free(*base);
	*base = tmp;
}
