/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   append.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 10:50:39 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 10:22:46 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "append.h"

void	append(char **base, char *append)
{
	char	*tmp;
	int		size;

	size = ft_strlen(*base) + ft_strlen(append) + 1;
	tmp = malloc(sizeof(char) * size);
	ft_bzero(tmp, size);
	ft_strcpy(tmp, *base);
	ft_strcat(tmp, append);
	free(*base);
	*base = tmp;
}
