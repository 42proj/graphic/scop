/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hsv_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 17:01:10 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 09:29:39 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "colors.h"
#include <stdlib.h>
#include <math.h>

t_rgb		hsv_0_to_120(float h, float c, float x, float m)
{
	t_rgb	rgb;

	if (0 <= h && h < 60)
	{
		rgb.r = (c + m) * 255;
		rgb.g = (x + m) * 255;
		rgb.b = (0 + m) * 255;
	}
	else
	{
		rgb.r = (x + m) * 255;
		rgb.g = (c + m) * 255;
		rgb.b = (0 + m) * 255;
	}
	return (rgb);
}

t_rgb		hsv_120_to_240(float h, float c, float x, float m)
{
	t_rgb	rgb;

	if (120 <= h && h < 180)
	{
		rgb.r = (0 + m) * 255;
		rgb.g = (c + m) * 255;
		rgb.b = (x + m) * 255;
	}
	else
	{
		rgb.r = (0 + m) * 255;
		rgb.g = (x + m) * 255;
		rgb.b = (c + m) * 255;
	}
	return (rgb);
}

t_rgb		hsv_240_to_360(float h, float c, float x, float m)
{
	t_rgb	rgb;

	if (240 <= h && h < 300)
	{
		rgb.r = (x + m) * 255;
		rgb.g = (0 + m) * 255;
		rgb.b = (c + m) * 255;
	}
	else
	{
		rgb.r = (c + m) * 255;
		rgb.g = (0 + m) * 255;
		rgb.b = (x + m) * 255;
	}
	return (rgb);
}

t_rgb		hsv_to_rgb_int(float h, float s, float v)
{
	float	c;
	float	x;
	float	m;

	if (h < 0 || h >= 360 || s < 0 || s > 1 || v < 0 || v > 1)
		return ((t_rgb){0, 0, 0});
	c = v * s;
	x = c * (1 - fabs(fmod((h / 60), 2) - 1));
	m = v - c;
	if (0 <= h && h < 120)
		return (hsv_0_to_120(h, c, x, m));
	else if (120 <= h && h < 240)
		return (hsv_120_to_240(h, c, x, m));
	else
		return (hsv_240_to_360(h, c, x, m));
}
