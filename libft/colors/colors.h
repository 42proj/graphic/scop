/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 17:21:12 by svilau            #+#    #+#             */
/*   Updated: 2018/11/15 09:30:08 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COLORS_H
# define COLORS_H

typedef struct		s_rgb
{
	int r;
	int g;
	int b;
}					t_rgb;

t_rgb				hsv_to_rgb_int(float h, float s, float v);

#endif
