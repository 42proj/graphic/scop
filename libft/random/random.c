/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   random.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svilau <svilau@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 12:15:47 by svilau            #+#    #+#             */
/*   Updated: 2018/11/13 12:59:07 by svilau           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <time.h>
#include <stdlib.h>

float			rand_in_range(float min, float max)
{
	return (min + (float)(rand() / (float)(RAND_MAX) * (max - min + 1)));
}
